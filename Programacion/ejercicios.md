# Programación
# Ejercicios de Pseudocódigo


### Ejercicio 1

* ((4 – 2) * (5 + 1)/2 ^2 - (4 + 3)

```
  ((4 - 2) * (5 + 1)/2 ^2 - (4 + 3)
  (   2    *    6   /  4  -    7
  (        12       /  4  -    7
  (                 3     -    7
  (                      -4
  Error: paréntesis no balanceados
```

* (6 + 3) > 8 & (6 – 1) * 2 < 8 | 2^3 == 8

```
  (6 + 3) > 8 & (6 - 1) * 2 < 8 | 2^3 == 8
     9    > 8 &    5    * 2 < 8 |  8  == 8
     9    > 8 &         10  < 8 |  8  == 8
          T   &             F   |     T
              F                 |     T
                                T
```

* (6 ^ 2 + (8 – 2))/7 + 35/2 – 8 * 5 / 4 * 2

```
  (6 ^ 2 + (8 - 2))/7 + 35/2 - 8 * 5 / 4 * 2
  (  36  +    6   )/7 +  17  - 40    / 4 * 2
  (      42       )/7 +  17  -       10  * 2
                   6  +  17  -           20
                      23     -           20
                             3
```

* 27 % 4 + 14 / 4

```
  27 % 4 + 14 / 4
     3   +    3
         6
```

* 37 / 4 ^ 2 – 2

```
  37 / 4 ^ 2 - 2
  37 /   16  - 2
     2       - 2
             0

```

* (7 * 3 – 4 * 2) ^ 2 / 4 * 2

```
  (7 * 3 - 4 * 2) ^ 2 / 4 * 2
  (21    -   8  ) ^ 2 / 4 * 2
        13        ^ 2 / 4 * 2
                  169 / 4 * 2
                      42  * 2
                          84
```

* 25 >= 7 & ¬(7 <= 2)

```
  25 >= 7 & ¬(7 <= 2)
      T   & ¬   F
      T   & T
          T

```

* 24 > 5 & 10 <= 10 | 10 == 5

```
  24 > 5 & 10 <= 10 | 10 == 5
     T   &    T     |    F
         T          |    F
                    T
```

* (10 >= 15 | 23 == 13) & ¬(8 == 8)

```
  (10 >= 15 | 23 == 13) & ¬(8 == 8)
  (   F     |    F    ) & ¬   T
            F           & F
                        F

```

* (¬(6 / 3 > 3) | 7 > 7 ) & (3 <= 9 / 2 | 2 + 3 <= 7 / 2)

```
  (¬(6 / 3 > 3) | 7 > 7 ) & (3 <= 9 / 2 | 2 + 3 <= 7 / 2)
  (¬(  2   > 3) | 7 > 7 ) & (3 <=   4   |   5   <=   3  )
  (¬       F    |   F   ) & (  T        |       F       )
  (T            |   F   ) & (  T        |       F       )
  (             T       ) & (           T               )
                          T

```

* ‘H’ < ‘J’ | ‘9’ == ‘7’

```
  'H' < 'J' | '9' == '7'
      T     |     F
            T
```

### Ejercicio 2
Escribir la siguiente expresión algorítmicamente: “Evaluar si el contenido de la variable precio es igual o superior a 500 € pero igual o inferior a 52.000 €”

```pseudocodigo
LEER precio
SI precio >= 500 & <= 5200 ENTONCES
  MOSTRAR "SI"
SI NO
  MOSTRAR "NO"
FIN SI
```

### Ejercicio 3
Escribir una expresión que indique si una persona está jubilada (con edad igual o superior a 65 años) conociendo su fecha de nacimiento en día, mes y año (Dnac, Mnac, Anac) y la fecha actual expresada en dia, mes y año (Dact, Mact, Aact).

```pseudocodigo
LEER Dact, Mact, Aact
LEER Dnac, Mnac, Anac

anios = Aact - Anac               // años que han pasado desde nacimiento
SI anios <  65 ENTONCES
  MOSTRAR "No Jubilado" ENTONCES
SI anios >  65
  MOSTRAR "Jubilado" ENTONCES
SI anios == 65 ENTONCES           // Cumple este año, comprobemos si ya ha cumplido o no
  SI Mact <  Mnac ENTONCES        // Cumplirá otro mes
    MOSTRAR "No Jubilado" ENTONCES
  SI Mact >  Mnac ENTONCES        // Cumplió otro mes
    MOSTRAR "Jubilado" ENTONCES
  SI Mact == Mnac ENTONCES        // Cumple este mes, comprobemos que día para ver si ya cumplió o no
    SI Dact <  Dnac ENTONCES      // Cumplirá otro día de este mes
      MOSTRAR "No Jubilado" ENTONCES
    SI Dact >=  Dnac ENTONCES     // Cumple hoy, o ya cumplió este mes
      MOSTRAR "Jubilado" ENTONCES
    FIN SI
  FIN SI
FIN SI
```

### Ejercicio 4
Diseñar un algoritmo que lea e imprima una serie de números distintos de cero. El algoritmo debe terminar con un valor cero que no se debe imprimir. Finalmente se desea obtener la cantidad de valores leídos distintos de cero.

```pseudocodigo
count = 0
LEER in
MIENTRAS in != 0 HACER
  count = count + 1
  LEER in
FIN MIENTRAS
MOSTRAR count
```

### Ejercicio 5
Escribir un algoritmo que lea cuatro números y a continuación escriba el mayor de los cuatro.

```pseudocodigo
count = 0
LEER num                    // Leer un número. De momento, es el mayor
MIENTRAS count < 4 HACER    // Leer 4 veces
  LEER aux
  SI aux > num ENTONCES     // Si el nuevo número es mayor que el mayor, se convierte en mayor
    num = aux
  FIN SI
  count = count + 1
END MIENTRAS
MOSTRAR num
```

### Ejercicio 6
Diseñar un algoritmo para calcular la velocidad (en metros/segundo) de los corredores de una carrera de 1500 metros. La entrada serán los segundos que darán el tiempo de cada corredor. Por cada corredor se imprimirá el tiempo así como la velocidad media. El bucle se ejecutara hasta introducir una entrada de 0 que será la marca de fin de entrada de datos.

```pseudocodigo
LEER segundos
MIENTRAS segundos != 0 HACER
  MOSTRAR segundos "s"
  MOSTRAR 1500/segundos "m/s"
  LEER segundos
FIN MIENTRAS
```

### Ejercicio 7
Diseñar un algoritmo para determinar si un número n es primo.

```pseudocodigo
LEER n
var = 2
esPrimo = false
MIENTRAS var < n & !esPrimo HACER
  SI n % var == 0 ENTONCES
    esPrimo = true
  FIN SI
FIN MIENTRAS

SI esPrimo ENTONCES
  MOSTRAR "Es primo"
SI NO
  MOSTRAR "No es primo"
FIN SI
```

### Ejercicio 8
Escribir un algoritmo que calcule la superficie de un triángulo en función de la base y la altura.

```pseudocodigo
LEER base
LEER altura
areaTriangulo = base * altura / 2
MOSTRAR areaTriangulo
```

### Ejercicio 9
Escribir un programa que calcule y visualice en pantalla el cuadrado y el cubo de un número introducido por teclado.

```pseudocodigo
LEER num
cuadrado = num * num
cubo = num * cuadrado
MOSTRAR cuadrado
MOSTRAR cubo
```

### Ejercicio 10
Algoritmo que lea una calificación numérica entre 0 y 10 y la transforme en calificación alfabética, escribiendo el resultado.

```pseudocodigo
LEER calificacionNum

SI calificacionNum == 0 ENTONCES
  calificacionAlf = "A"
SI calificacionNum == 1 ENTONCES
  calificacionAlf = "B"
SI calificacionNum == 2 ENTONCES
  calificacionAlf = "C"
SI calificacionNum == 3 ENTONCES
  calificacionAlf = "D"
SI calificacionNum == 4 ENTONCES
  calificacionAlf = "E"
SI calificacionNum == 5 ENTONCES
  calificacionAlf = "F"
SI calificacionNum == 6 ENTONCES
  calificacionAlf = "G"
SI calificacionNum == 7 ENTONCES
  calificacionAlf = "H"
SI calificacionNum == 8 ENTONCES
  calificacionAlf = "I"
SI calificacionNum == 9 ENTONCES
  calificacionAlf = "J"
SI calificacionNum ==10 ENTONCES
  calificacionAlf = "K"
FIN SI

MOSTRAR calificacionAlf
```

### Ejercicio 11
Introducir un número por teclado y decir si es positivo, negativo o cero.

```pseudocodigo
LEER num
SI num < 0 ENTONCES
  MOSTRAR "Negativo"
SI num > 0 ENTONCES
  MOSTRAR "Positivo"
SI NO
  MOSTRAR "Cero"
FIN SI
```

### Ejercicio 12
Diseñar una función que permita obtener el valor absoluto de un número.

```pseudocodigo
LEER num
SI num >= 0 ENTONCES
  abs = num
SI NO
  abs = -num
FIN SI
MOSTRAR abs
```

### Ejercicio 13
Introducir números por teclado hasta que la suma de todos ellos sea mayor que 1000.

```pseudocodigo
sum = 0

MIENTRAS sum <= 1000 HACER
  LEER num
  sum = sum + num
FIN MIENTRAS

MOSTRAR sum
```

### Ejercicio 14
Hallar la suma de los múltiplos de 5 comprendidos entre 1 y 100. Calcular además cuántos hay y visualizar cada uno de ellos.

```pseudocodigo
multiploDe = 5
inicio = 1
fin = 100

sum = 0
count = 0
i = inicio
MINETRAS i <= 100 HACER
  SI i % multiploDe == 0 ENTONCES
    MOSTRAR "Múltiplo: " i
    sum = sum + i
    count = count + 1
  FIN SI
  i = i + 1
FIN MIENTRAS

MOSTRAR "Cuenta: " count
MOSTRAR "Suma: " sum
```

### Ejercicio 15
Introducir las notas de FP obtenidas por 40 alumnos de una clase y contar cuántos de ellos han aprobado.

```pseudocodigo
aprobados = 0
i = 0
MIENTRAS i < 40 HACER
  LEER nota
  SI nota >= 5 ENTONCES
    aprobados = aprobados + 1
  FIN SI
  i = i + 1
FIN MIENTRAS
MOSTRAR aprovados
```

### Ejercicio 16
Calcular la suma de los cuadrados de los 400 primeros números naturales.

```pseudocodigo
sum = 0
i = 1
MIENTRAS i <= 400 HACER
  sum = sum + i*i
FIN MIENTRAS
MOSTRAR sum
```

### Ejercicio 17
Algoritmo que lee como dato de entrada un año y nos dice si se trata de un año bisiesto o no. Se sabe que son bisiestos todos los años múltiplos de 4, excepto los que sean múltiplos de 100 sin ser múltiplos de 400.

```pseudocodigo
LEER year

SI year % 4 == 0 & ( year % 100 != 0 | year % 400 == 0 ) ENTONCES
  MOSTRAR "Es bisiesto"
SI NO
  MOSTRAR "No es bisiesto"
FIN SI
```

### Ejercicio 18
Realizar 10 veces el proceso siguiente: Introducir un número del 1 al 50 y visualizar a qué decena pertenece.

```pseudocodigo
i = 0
MIENTRAS i < 50 HACER
  LEER num
  MOSTRAR num % 10
  i = i + 1
FIN MIENTRAS
```

### Ejercicio 19
Diseñar un algoritmo que lea 200 números y determine si cada número leído está comprendido en el intervalo 35-75, mostrando la palabra ‘Cierto’ en caso afirmativo y ‘Falso’ en caso negativo.

```pseudocodigo
i = 0
MIENTRAS i < 200 HACER
  LEER num
  SI num >= 35 & num <= 75 ENTONCES
    MOSTRAR "Cierto"
  SI NO
    MOSTRAR "Falso"
  FIN SI
  i = i + 1
FIN MIENTRAS
```

### Ejercicio 20
Desarrollar un algoritmo que determine en un conjunto de cien números naturales cuántos son menores de 15, cuántos son mayores de 50 y cuántos están comprendidos entre 25 y 45

```pseudocodigo
menores15  = 0
mayores50  = 0
entre25y45 = 0

i = 0
MIENTRAS i < 100 HACER
  LEER num
  SI num < 15 ENTONCES
    menores15 = menores15 + 1
  SI num >= 25 & num <= 45 ENTONCES
    entre25y45 = entre25y45 + 1
  SU num > 50 ENTONCES
    mayores50 = mayores50 + 1
  FIN SI
  i = i + 1
FIN MIENTRAS

MOSTRAR "Menores de 15: " menores15
MOSTRAR "Mayores de 50: " mayores50
MOSTRAR "Entre 25 y 75: " entre25y45
```

### Ejercicio 21
Introducir las estaturas en cm de los 40 alumnos de una clase, imprimir la mayor de ellas y la estatura media.

```pseudocodigo
mayor = 0
sum = 0

i = 0
MIENTRAS i < 40 HACER
  LEER estatura
  sum = sum + estatura
  SI estatura > mayor ENTONCES
    mayor = estatura
  FIN SI
  i = i + 1
FIN MIENTRAS

MOSTRAR "Mayor: " mayor  "cm"
MOSTRAR "Media: " sum/40 "cm"
```

### Ejercicio 22
Calcular y visualizar la suma y el producto de los números pares comprendidos entre 20 y 400 ambos inclusive.

```pseudocodigo
sum = 0
prod = 1

i = 20
MIENTRAS i <= 400 HACER
  SI i % 2 == 0 ENTONCES
    sum = sum + i
    prod = prod * i
  FIN SI
  i = i + 1
FIN MIENTRAS

MOSTRAR "Suma: " sum
MOSTRAR "Producto: " prod
```

### Ejercicio 23
Calcular los N primeros múltiplos de 4 donde N es un valor introducido por teclado.

```pseudocodigo
LEER n

count = 0
i = 0
MIENTRAS count < n HACER
  SI i % 4 == 0 ENTONCES
    MOSTRAR i
    count = count + 1
  FIN SI
  i = i + 1
FIN MIENTRAS
```

### Ejercicio 24
Imprimir las primeras treinta potencias de 4.

```pseudocodigo
num = 1
i = 0
MIENTRAS i < 30 HACER
  num = num * 4
  MOSTRAR num
  i = i + 1
FIN SI
```

### Ejercicio 25
Diseñar el algoritmo de un programa que escribe la tabla de multiplicar de un número introducido por teclado.

```pseudocodigo
LEER num
i = 0
MIENTRAS i <= 10
  MOSTRAR num "*" i "=" num*i
  i = i + 1
FIN SI
```

### Ejercicio 26
Algoritmo que nos pide la introducción por teclado de las notas de una clase de 40 alumnos y nos calcula el tanto por ciento de suspendidos.

```pseudocodigo
aprobados = 0
i = 0
MIENTRAS i < 40 HACER
  LEER nota
  SI nota >= 5 ENTONCES
    aprobados = aprobados + 1
  FIN SI
  i = i + 1
FIN MIENTRAS
suspendidos = 40 - aprobados
MOSTRAR suspendidos / 40 * 100
```

### Ejercicio 27
Diseñar un programa que muestre la suma de los números impares comprendidos entre dos valores numéricos enteros y positivos introducidos por teclado.

```pseudocodigo
LEER a, b
SI a > b ENTONCES         // Si a es mayor, intercambiar (para tener el menor en a)
  aux = b
  b = a
  a = aux
FIN SI

sum = 0
i = a
MIENTRAS i <= b HACER     // calcular suma de impares desde a hasta b
  SI i % 2 == 1 ENTONCES
    sum = sum + i
  FIN SI
FIN MINETRAS

MOSTRAR sum
```

### Ejercicio 28
Solicite 10 números positivos menores que 100 por teclado y calcule la media aritmética de los que sean múltiplos de 6.

```pseudocodigo
sumMul6 = 0
countMul6 = 0

count = 0
MIENTRAS count < 10 HACER          // Hasta conseguir 10 números válidos
  LEER num
  SI num > 0 & num < 100 ENTONCES  // Si el número es válido
    SI num % 6 == 0 ENTONCES       // Si es múltiplo de 6
      sumMul6 = sumMul6 + num      // Se añade al sumatorio
      countMul6 = countMul6 + 1    // Se incrementa la cantidad de múltiplos de 6
    FIN SI
    count = count + 1
  FIN SI
FIN MIENTRAS

MOSTRAR sumMul6/countMul6
```

### Ejercicio 29
Programa que lea los nombres y las edades de varias personas y escriba las de mayor y menor edad (considerando que no puede haber coincidencia de edades). El programa finalizará al introducir espacios en el nombre.


```pseudocodigo
nombreMayor
nombreMenor
edadMayor
edadMenor

LEER nombre, edad               // Leer primera persona
nombreMayor = nombre
nombreMenor = nombre
edadMayor = edad
edadMenor = edad

MIENTRAS nombre != " " HACER   // Minetras que el nombre no sea "espacio"
  SI edad < edadMenor ENTONCES // Si su edad es menor que la menor, se conviernte en la menor
    nombreMenor = nombre
    edadMenor = edad
  FIN SI
  SI edad > edadMayor ENTONCES // Si su edad es mayor que la mayor, se conviernte en la mayor
    nombreMayor = nombre
    edadMayor = edad
  FIN SI
  LEER nombre, edad
FIN MIENTRAS

MOSTRAR "Mayor: " nombreMayor " - " edadMayor
MOSTRAR "Menor: " nombreMenor " - " edadMenor
```

### Ejercicio 30
Programa que lee dos números y presenta una serie de opciones correspondientes a distintas operaciones que podemos realizar con ellos (suma, resta, multiplicación y división), de manera que en función de la opción elegida muestra el resultado de la operación realizada. En aquellos casos en los que se desee seguir operando con los mismos números, se debe contestar ‘S’ al siguiente mensaje: "¿Otra operación con los mismos números S/N?", o ‘N’ en caso contrario. Finalmente el programa deberá concluir tras responder ‘S’ o ‘N’ a la siguiente pregunta: "¿Terminar (S/N)?".

```pseudocodigo
quit = false
MIENTRAS !quit ENTONCES
  continuar = true
  MOSTRAR "Elija 2 números con los que operar: "
  LEER a, b
  MIENTRAS continuar HACER

    MOSTRAR "¿Operación? (+-*/)"
    LEER input
    SI input == "+" ENTONCES
      resultado = a + b
    SI input == "-" ENTONCES
      resultado = a - b
    SI input == "*" ENTONCES
      resultado = a * b
    SI input == "/" ENTONCES
      resultado = a / b
    FIN SI
    MOSTRAR resultado

    MOSTRAR "¿Otra operación con los mismos números S/N?"
    LEER input
    SI input == "N" ENTONCES
      continuar = false
    FIN SI
  FIN MIENTRAS

  MOSTRAR "¿Terminar (S/N)?"
  LEER input
  SI input == "S" ENTONCES
    quit = true
  FIN SI
END MIENTRAS
```

### Ejercicio 31
Programa que lee números y escribe cuántos números positivos y negativos se han leído. La finalización de la entrada de datos se realiza por la respuesta al siguiente mensaje: "¿Terminar (S/N)?", escrito después de introducir cada número.

```pseudocodigo
quit = false
positives = 0
negatives = 0
MIENTRAS !quit HACER
  LEER num
  SI num < 0 ENTONCES
    negatives = negatives + 1
  SI num > 0 ENTONCES
    positives = positives + 1
  FIN SI

  MOSTRAR "¿Terminar (S/N)?"
  LEER input
  SI input == "S" ENTONCES
    quit = true
  FIN SI
FIN MIENTRAS

MOSTRAR "Positivos: " positives
MOSTRAR "Negativos: " negatives
```
